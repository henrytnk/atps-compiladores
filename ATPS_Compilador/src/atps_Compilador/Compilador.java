package atps_Compilador;

import java.util.ArrayList;
import java.util.List;

public class Compilador {
/* ********************************************************************************************************************************************** */	
//														===========================================
//	 													|| CENTRO UNIVERSITARIO ANHANGUERA UNAES ||
//	 													||				08.10.2014	     		 ||	
//														||			   HENRY TANAKA              ||
//														||			   RA 2554445738             ||
//	 													===========================================
//	
//	*************************************
//	******FUNCIONAMENTO DO PROGRAMA******
//	*************************************
//	
//	EX: inteiro var;
//	
//	         ||		APOS APLICARMOS
//		     \/		    SPLIT:
//	
//	splitted[0] -> inteiro 	
//	splitted[1] -> var 	
//	splitted[2] -> ; 	
//	
//	         ||		VERIFICANDO	PRIMEIRO 
//	         \/			SPLITTED:
//
//	VARIAVEL ACUMULADORA:		STATUS:
//	i							-> NAO RECONHECIDO, CONTINUA PERCORRENDO
//	in							-> NAO RECONHECIDO, CONTINUA PERCORRENDO
//	int							-> NAO RECONHECIDO, CONTINUA PERCORRENDO
//	inte						-> NAO RECONHECIDO, CONTINUA PERCORRENDO
//	intei						-> NAO RECONHECIDO, CONTINUA PERCORRENDO
//	inteir						-> NAO RECONHECIDO, CONTINUA PERCORRENDO
//	inteiro						-> TERMO RECONHECIDO -> IMPRIME TERMO E SIGNIFICADO
//	
//  	    ||		CONTINUA ATE TER EXAMINADO TODOS
//          \/		     OS TOKENS EXISTENTES	
//	
//	------------------	
//	|RESULTADO FINAL:|
//	------------------	
//	inteiro -> TIPO PRIMITIVO
//	var -> NOME VARIAVEL
//	; -> DELIMITADOR FIM DE LINHA
//	
//  		||		PASSA PARA A LINHA
// 		    \/			SEGUINTE
//	
// 		    ...		E ASSIM POR DIANTE
// 			...     	  [...]
//	
//	*************************************
//	********TRATAMENTOS ESPECIAIS********
//	*************************************	
//	
//	CASO 1: CRIACAO DE NOVA VARIAVEL
//	
//	EX: inteiro tempo;
//	
//	PASSOS:	
//  1. APOS O TERMO "inteiro" SER RECONHECIDO, A VARIAVEL BOOLEANA "novaVariavel" SERA SETADA COMO TRUE
//	2. ENQUANTO "novaVariavel" FOR TRUE, O ACUMULADOR CONTINUARA PERCORRENDO O NOME DA VARIAVEL
//	3. SE "novaVariavel" FOR TRUE E UMA VIRGULA FOR DETECTADA, O NOME DA VARIAVEL SERA ADICINADO NA LISTA DE VARIAVEIS (CASO AINDA NAO ESTEJA) 
//	   E O PROGRAMA CONTINUARA PERCORRENDO EM BUSCA DE NOVAS VARIAVEIS
//	4. SE "novaVariavel" FOR TRUE E UM PONTO E VIRGULA FOR DETECTADO, O NOME DA VARIAVEL SERA ADICINADO NA LISTA DE VARIAVEIS (CASO AINDA NAO
//	   ESTEJA) E "novaVariavel" SERA SETADA COMO FALSE	
//	
//	
//	CASO 2: RECONHECENDO TERMOS "++", "--", "=="	
//	
//	EX: tempo--;
//		
//	PASSOS:
//	1. APOS O PRIMEIRO "-" SER RECONHECIDO, O PROGRAMA VERIFICARA O PROXIMO CARACTERE DA STRING
//	2. CASO O PROXIMO SEJA DIFERENTE DE "-", SERA IMPRESSO O TERMO E SEU SIGNIFICADO ("-" SERA RECONHECIDO COMO OPERADOR ARITMETICO)
//	3. CASO O CONTRARIO, O PROGRAMA IRA GUARDAR O CARACTERE ATUAL ("-") NA STRING ACUMULADORA E PROSSEGUIRA PARA O PROXIMO PASSO
//	   COMO A STRING ACUMULOU CORRETAMENTE DOIS "-", O TERMO SERA RECONHECIDO DEVIDAMENTE COMO "OPERADOR SUFIXAL"		
//	
//	
//	CASO 3: RECONHECENDO CONTEUDO DENTRO DE UMA STRING	
//	
//	EX: string mensagem = "hello world";
//		
//	PASSOS:
//	1. APOS A PRIMEIRA ASPA SER DETECTADA, A VARIAVEL "aspas" SERA SETADA COMO TRUE
//	2. ENQUANTO "aspas" FOR TRUE, TODO CONTEUDO LIDO SERA ARMAZENADO NA VARIAVEL "conteudoString"
//	3. APOS A SEGUNDA ASPA SER DETECTADA, A VARIAVEL "aspas" SERA SETADA COMO FALSE E O "conteudoString" SERA DEVIDAMENTE
//	   RECONHECIDO COMO CONTEUDO DE UMA STRING		
//		
//		
//	CASO 4: RECONHECENDO TIPO INTEIRO
//	
//	EX: inteiro tempo = 60;
//		
//	PASSOS:
//	1. APOS O PRIMEIRO NUMERO SER DETECTADO, O PROGRAMA IRA CONFERIR SE O PROXIMO DIGITO A SER ANALISADO TAMBEM EH UM NUMERO
//	2. CASO O PROXIMO TAMBEM SEJA UM NUMERO, O ACUMULADOR IRA ACUMULAR O NUMERO ATUAL E SEGUIR PARA O PROXIMO PASSO
//	3. CASO CONTRARIO, O ACUMULADOR IRA REGISTRAR O NUMERO ATUAL E IMPRIMIR O TOTAL ACUMULADO
//	   
/* ********************************************************************************************************************************************** */	
	
	private static List<Token> listaToken = new ArrayList<>();		/* LISTA QUE IRA CONTER TODOS OS TOKENS DA LINGUAGEM */
	private static List<Variavel> listaVariavel = new ArrayList<>();/* LISTA QUE IRA CONTER TODAS AS VARIAVEIS CRIADAS */
	private static String numeros = "^[0-9]*$";						/* STRING USADA PARA A VERIFICAO DE NUMEROS */
	private static String delimitador = "[;+-]";					/* STRING USADA PARA A VERIFICAO DE NUMEROS */
	private static String soma = "= [;+-]";							/* STRING USADA PARA A VERIFICAO DE NUMEROS */
	private static String tipo;										/* STRING USADA PARA GUARDAR O TIPO DE VARIAVEL */
	private static String varAtual;									/* STRING USADA PARA GUARDAR A VARIAVEL SENDO UTILIZADA ATUALMENTE */
	private static int nLinha = 1;									/* VARIAVEL QUE SE AUTO INCREMENTA CONFORME O NUMERO DA LINHA */
	private static boolean aspas = false;							/* VARIAVEL USADA PARA DETECTAR ABRIMENTO E FECHAMENTO DE ASPAS */
	private static boolean novaVariavel = false;					/* VARIAVEL USADA PARA DETECTAR CRIACAO DE NOVA VARIAVEL */
	private static boolean passandoValor = false;					/* VARIAVEL USADA PARA DETECTAR PASSAGEM DE VALOR PARA UMA VARIAVEL */
	
	public static void main(String[] args) {
		preencherListaToken();										/* PRIMEIRO PASSO: PRENCHER A LISTA DE TOKENS DA LINGUAGEM */
		Leitor.escanear();											/* SEGUNDO PASSO: ESCANEAR O ARQUIVO FONTE A SER LIDO */
		imprimirLog();												/* TERCEIRO PASSO: CONFERIR E IMPRIMIR O RESULTADO DO ESCANEAMENTO LINHA POR LINHA */
	}

	private static void preencherListaToken() {
		/* TIPOS PRIMITIVOS */
		listaToken.add(new Token("TIPO PRIMITIVO", "inteiro"));
		listaToken.add(new Token("TIPO PRIMITIVO", "booleano"));
		listaToken.add(new Token("TIPO PRIMITIVO", "string"));
		
		/* ENQUANTO */
		listaToken.add(new Token("LACO DE REPETICAO ENQUANTO", "enquanto"));

		/* CONDICAO */
		listaToken.add(new Token("CONDICIONAL", "se"));
		listaToken.add(new Token("DELIMITADOR DE ESCOPO CONDICIONAL", "entao"));
		listaToken.add(new Token("NEGACAO DE CONDICAO", "senao"));
		listaToken.add(new Token("DELIMITADOR DE FIM DE ESCOPO CONDICIONAL", "fimSe"));
		
		/* PARA */
		listaToken.add(new Token("LACO DE REPETICAO PARA", "para"));
		listaToken.add(new Token("CONDICIONAL-REPETICAO", "ate"));
		listaToken.add(new Token("DELIMITADOR DE ESCOPO DE REPETICAO", "faca"));
		listaToken.add(new Token("DELIMITADOR DE FIM DE ESCOPO DE REPETICAO", "fimRepeticao"));

		/* ACOES */
		listaToken.add(new Token("ACAO", "leia"));
		listaToken.add(new Token("ACAO", "escreva"));
		listaToken.add(new Token("ACAO", "aquecer"));
		listaToken.add(new Token("ACAO", "finalizar"));

		/* PARENTESES */
		listaToken.add(new Token("DELIMITADOR PARENTESES", "("));
		listaToken.add(new Token("DELIMITADOR FECHA PARENTESES", ")"));
		
		/* OPERADORES RELACIONAIS */
		listaToken.add(new Token("OPERADOR RELACIONAL", "<"));
		listaToken.add(new Token("OPERADOR RELACIONAL", ">"));
		listaToken.add(new Token("OPERADOR RELACIONAL", "<="));
		listaToken.add(new Token("OPERADOR RELACIONAL", ">="));
		listaToken.add(new Token("OPERADOR RELACIONAL", "=="));
		listaToken.add(new Token("OPERADOR RELACIONAL", "="));

		/* OPERADORES SUFIXAIS */
		listaToken.add(new Token("OPERADOR SUFIXAL", "++"));
		listaToken.add(new Token("OPERADOR SUFIXAL", "--"));
		
		/* OPERADORES ARITIMETICOS */
		listaToken.add(new Token("OPERADOR ARITIMETICO", "+"));
		listaToken.add(new Token("OPERADOR ARITIMETICO", "-"));
		listaToken.add(new Token("OPERADOR ARITIMETICO", "*"));
		listaToken.add(new Token("OPERADOR ARITIMETICO", "/"));

		/* OPERADORES LOGICOS */
		listaToken.add(new Token("OPERADOR LOGICO", "!="));
		listaToken.add(new Token("OPERADOR LOGICO", "!"));
		listaToken.add(new Token("OPERADOR LOGICO", "&&"));
		listaToken.add(new Token("OPERADOR LOGICO", "|"));
		listaToken.add(new Token("OPERADOR LOGICO", "!"));
		listaToken.add(new Token("OPERADOR LOGICO", "verdadeiro"));
		listaToken.add(new Token("OPERADOR LOGICO", "falso"));

		/* OPERADORES DE ATRIBUICAO */
		listaToken.add(new Token("OPERADOR DE ATRIBUICAO", "="));
		
		/* DELIMITADOR DE LINHA */
		listaToken.add(new Token("DELIMITADOR FIM DE LINHA", ";"));

		/* DELIMITADORES DE ESCOPO */
		listaToken.add(new Token("DELIMITADOR DE ESCOPO", "inicio"));
		listaToken.add(new Token("DELIMITADOR DE ESCOPO", "fimCodigo"));

		/* OUTROS */
		listaToken.add(new Token("DELIMITADOR VIRGULA", ","));
		listaToken.add(new Token("DELIMITADOR DE STRING", "\""));
	}

//	@SuppressWarnings("null")
	private static void imprimirLog() {
		for (int x = 0; x < Leitor.linhaCodigo.size(); x++) {
			String linha = Leitor.linhaCodigo.get(x); 				/* RECEBE A LINHA DA CLASSE LEITORA */
			String[] splitted = linha.split(" ");    	 			/* QUEBRA A LINHA DE ACORDO COM O 'ESPACO' */
			String conteudoString = "";              				/* VARIAVEL QUE ARMAZENA O CONTEUDO DA STRING */ 
			String acumulador = "";     							/* VARIAVEL QUE ACUMULA OS CARACTERES A SEREM EXAMINADOS PELO ANALISADOR LEXICO */
			
			System.out.println("Linha " + nLinha + ": ");
			
			for (int y = 0; y < splitted.length; y++) {
				String token = splitted[y];							/* VARIAVEL QUE RECEBE SEQUENCIALMENTE CADA PARTE "QUEBRADA" ANTERIORMENTE */
				
				for (int z = 0; z < token.length(); z++) {
					String caractere = token.substring(z, z+1); 	/* VARIAVEL QUE RECEBE CARACTERE POR CARACTERE A SER EXAMINADO */
					
					/* TRATAMENTO 1: CRIACAO DE NOVA VARIAVEL *///G:\microondas.txt
					if(novaVariavel == true && !caractere.equals(",") && !caractere.equals(";") && !caractere.equals("=") && !acumulador.matches("\\d+")) {
						acumulador = acumulador.concat(caractere); 
					} else if(novaVariavel == true && (caractere.equals(",") || caractere.equals("=") || caractere.equals(";"))) {
						if (!checarVariavel(acumulador) && !acumulador.isEmpty() && !acumulador.substring(0,1).matches(numeros)) listaVariavel.add(new Variavel (tipo, acumulador));
						else if (checarVariavel(acumulador)) {System.out.println("!ERRO SEMANTICO: VARIAVEL " + acumulador +" JA DECLARADA ANTERIORMENTE!"); acumulador = "";}
						if (caractere.equals("=")) { tipo = ""; passandoValor = true; }
						analisadorLexico(acumulador);
						analisadorLexico(caractere);
						acumulador = "";
						if (caractere.equals(";")) { novaVariavel = false; tipo = ""; passandoValor = false; analisadorSintatico1(linha);}
					} 
					
					else if (novaVariavel == true && token.matches(numeros)) {
						System.out.println(token + " -> VALOR TIPO INTEIRO");
						System.out.println("!!!ERRO SINTATICO!!!");
						novaVariavel = false;
						acumulador = ""; tipo = ""; token = "";
					}
 					
					/* TRATAMENTO 2: RECONHECENDO "++", "--", "==" */
					else if(caractere.equals("=") || caractere.equals("-") || caractere.equals("+")	){					   	
						if (token.length() > z + 1 && (token.substring(z+1, z+2).equals("=")	|| 
													   token.substring(z+1, z+2).equals("+")	|| 
													   token.substring(z+1, z+2).equals("-")	)) 
													    acumulador = acumulador.concat(caractere);
						else {acumulador = acumulador.concat(caractere); analisadorLexico(acumulador); acumulador = "";}
					} 
					
					/* TRATAMENTO 3: RECONHECENDO CONTEUDO DENTRO DE UMA STRING */ //G:\microondas.txt
					else if (caractere.equals("\"") && !aspas) {			
						analisadorLexico(caractere);
						aspas = true; 
					} else if (!caractere.equals("\"") && aspas) {
						conteudoString = conteudoString.concat(caractere);
						if (token.length() == z + 1) conteudoString = conteudoString.concat(" ");
					} else if (caractere.equals("\"") && aspas) {
						System.out.println(conteudoString + " -> CONTEUDO STRING");
						conteudoString = "";
						analisadorLexico(caractere);
						aspas = false;
					}
					
					/* TRATAMENTO 4: RECONHECENDO TIPO INTEIRO */ // G:\microondas.txt
					else if(caractere.matches(numeros)){					   	
						if (token.length() > z + 1 && token.substring(z+1, z+2).matches(numeros)) acumulador = acumulador.concat(caractere); 
						else {acumulador = acumulador.concat(caractere); analisadorLexico(acumulador); acumulador = "";}	
						
					/* RECONHECER TOKEN ACUMULADO */	// G:\microondas.txt
					} else {
						acumulador = acumulador.concat(caractere);
						
						if ((acumulador.equals("inteiro") || acumulador.equals("booleano") ||acumulador.equals("string"))) {
							novaVariavel = true;
							tipo = acumulador;
						} else if (token.length() > z + 1 && token.substring(z+1, z+2).equals("=")) {
							varAtual = acumulador;
							System.out.println("VARIAVEL ATUAL ------> " + varAtual);
						}
						
						if (analisadorLexico(acumulador)) acumulador = "";						
						else if (!acumulador.equals("") && token.length() == z + 1) { System.out.println(acumulador + " -> termo nao reconhecido"); acumulador = ""; }
					}
				}
			}
 			
 			System.out.println();
			nLinha++;
		}
		
		System.out.println("---LISTA DE VARIAVEIS---");
		imprimirLista();
		System.out.println("==========================================\n--FIM DO PROGRAMA--");
	}

	private static void analisadorSintatico1(String linha) {
//		System.out.println("LINHA " + linha);
		String[] splitted = linha.split(" ");
		
		if (splitted[0].equals("inteiro") || splitted[0].equals("string") || splitted[0].equals("boolean")) {
			if (checarVariavel(splitted[1])) {
				if (splitted[2].equals("=")) {
					String[] aux = splitted[3].split(";");
					if (aux[0].matches(numeros)) {
						System.out.println("---ATRIBUICAO FEITA COM SUCESSO---");
					}
				}
			}
		} else System.out.println("ERRO NA DECLARACAO DA VARIAVEL!!!");
	} // G:\microondas.txt
	
	private static void analisadorSintatico2(String linha) {
//		System.out.println("LINHA " + linha);
		String[] splitted = linha.split(" ");
		
		if (splitted[0].equals("inteiro") || splitted[0].equals("string") || splitted[0].equals("boolean")) {
			if (checarVariavel(splitted[1])) {
				if (splitted[2].equals("=")) {
					if (splitted[3].matches(numeros)) {
						if (splitted[4].equals("+") || splitted[4].equals("-")){
							String[] aux = splitted[5].split(";");
							if (aux[0].matches(numeros)) {
								System.out.println("---ATRIBUICAO FEITA COM SUCESSO---");
							}
						}
					}
				}
			}
		}
	} // G:\microondas.txt

	private static boolean analisadorLexico(String acumulador) { 	/* VERIFICA E IMPRIME EXISTENCIA DE TOKEN OU VARIAVEL DENTRO DO ACUMULADOR*/
		acumulador = acumulador.replace("	", "");
		
		for (int i = 0; i < listaToken.size(); i++) {
			String token = listaToken.get(i).getTipo();
			String termo = listaToken.get(i).getTermo();
		
			if (acumulador.contains(token) && !acumulador.matches(numeros)) {
				String aux = acumulador;
				acumulador = acumulador.replace(token, "");
				if (!acumulador.isEmpty() && acumulador.matches(numeros)) System.out.println(acumulador + " -> VALOR TIPO INTEIRO"); 
				else if (!acumulador.isEmpty()) System.out.println(acumulador + " -> termo nao reconhecido"); 
				System.out.println(token + " -> " + termo); 
				return true;
			} 
			else if (!acumulador.isEmpty() && checarVariavel(acumulador)) {
				imprimirVariavel(acumulador);
//				System.out.println(acumulador + " -> NOME VARIAVEL");
				return true;
			} 
		else if (!acumulador.isEmpty() && acumulador.matches(numeros)) {
				System.out.println(acumulador + " -> VALOR TIPO INTEIRO");
				if (passandoValor == true) {
//					System.out.println("VALOR ACUMULADO " + acumulador);
					listaVariavel.get(listaVariavel.size() - 1).setValor(Integer.parseInt(acumulador));
				}
				return true;
			} 
		}
		return false;
	}

	private static boolean checarVariavel(String variavel) { 	/* VERIFICA A EXISTENCIA DA VARIAVEL DENTRO DA LISTA DE VARIAVEIS*/
		for (int i = 0; i < listaVariavel.size(); i++) {
			if (listaVariavel.get(i).getNome().equals(variavel)) {
				return true;
			}
		}
		return false;
	}
	
	private static void imprimirVariavel(String variavel) {
		for (int i = 0; i < listaVariavel.size(); i++) {
			if (listaVariavel.get(i).getNome().equals(variavel)) {
				listaVariavel.get(i).printVar();
			}
		}
	}
	
	private static void imprimirLista() {
		for (int i = 0; i < listaVariavel.size(); i++) {
			System.out.println("VARIAVEL: " + listaVariavel.get(i).getNome() + " VALOR: " + listaVariavel.get(i).getValor());
		}
	}
	
	private static void compararTipos() {
		
	}
} // G:\microondas.txt