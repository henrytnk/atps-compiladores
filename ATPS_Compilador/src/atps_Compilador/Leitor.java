package atps_Compilador;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Leitor {
	private static Scanner scan = new Scanner(System.in);
	private static String endereco;
	public static List<String> linhaCodigo = new ArrayList<>();
	
	public static void escanear() {
		System.out.print("Informe o diretorio do arquivo a ser interpretado: ");
		endereco = scan.nextLine();
		System.out.println("==========================================");
		conferir();
	}
	
	private static void conferir() {
		try {
			FileReader arq = new FileReader(endereco);
			BufferedReader leitor = new BufferedReader(arq);

			String linha = leitor.readLine();

			while (linha != null) {
				linhaCodigo.add(linha);
				linha = leitor.readLine();
			}

			arq.close();
		} catch (IOException e) {
			System.err.printf("ERRO DE COMPILACAO",	e.getMessage());
		}
	}
}
