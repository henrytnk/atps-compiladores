package atps_Compilador;

public class Token {
	private String termo;
	private String tipo;
	
	public Token(String termo, String tipo) {
		this.termo = termo;
		this.tipo = tipo;
	}

	public String getTermo() {
		return termo;
	}

	public String getTipo() {
		return tipo;
	}
}
