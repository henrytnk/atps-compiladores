package atps_Compilador;

public class Variavel {
	private int valor;
	private String texto, nome, tipo;

	public Variavel(String tipo, String nome) {
		this.tipo = tipo;
		this.nome = nome;
//		printVar();
	}

	public void printVar() {
		System.out.println(nome + " -> VARIAVEL DO TIPO " + tipo.toUpperCase());
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
