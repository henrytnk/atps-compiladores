## Trabalho de Compiladores

Esse trabalho foi desenvolvido como parte da disciplina de "Compiladores" da Universidade Anhanguera de Campo Grande. O objetivo é criar um analisador sintático e léxico que reconheça os tokens gerado em um arquivo fonte (`'microondas.txt'`) e saiba interpretar corretamente todas as variáveis e instruções apresentadas.